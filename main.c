/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	init(t_env *e)
{
	e->coll = 0;
	e->ambiant_light = 0.15;
	e->camera = (t_camera *)malloc(sizeof(t_camera));
	e->camera->pos.x = 0.0;
	e->camera->pos.y = 0.0;
	e->camera->pos.z = 0.0;
	e->camera->dir.x = 0.0;
	e->camera->dir.y = 0.0;
	e->camera->dir.z = 1.0;
	e->camera->dir = normalize(&e->camera->dir);
	e->light = (t_light *)malloc(sizeof(t_light));
	e->light->pos.x = 5.0;
	e->light->pos.y = -3.0;
	e->light->pos.z = 0.0;
	e->ray = (t_ray *)malloc(sizeof(t_ray));
	e->ray->origin.x = e->camera->pos.x;
	e->ray->origin.y = e->camera->pos.y;
	e->ray->origin.z = e->camera->pos.z;
	e->clsn = (t_collision *)malloc(sizeof(t_collision));
	e->clsn->impact_d = -1.0;
	e->spheres = NULL;
	e->planes = NULL;
	e->cylinders = NULL;
	e->cones = NULL;
}

void	init_scene(t_env *e)
{
	if (e->n_scene == 1)
		scene_1(e);
	else if (e->n_scene == 2)
		scene_2(e);
	else if (e->n_scene == 3)
		scene_3(e);
	else if (e->n_scene == 4)
		scene_4(e);
	else
		scene_5(e);
}

int		expose_hook(t_env *e)
{
	mlx_put_image_to_window(e->mlx_ptr, e->win_ptr, e->img_ptr, 0, 0);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
		quit_program("End of program", e);
	if (keycode == KEY_UP)
		e->light->pos.y -= 1.0;
	if (keycode == KEY_DOWN)
		e->light->pos.y += 1.0;
	if (keycode == KEY_LEFT)
		e->light->pos.x -= 1.0;
	if (keycode == KEY_RIGHT)
		e->light->pos.x += 1.0;
	trace(e);
	mlx_put_image_to_window(e->mlx_ptr, e->win_ptr, e->img_ptr, 0, 0);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env	*e;

	e = (t_env *)malloc(sizeof(t_env));
	parser(argc, argv, e);
	init(e);
	init_scene(e);
	if ((e->mlx_ptr = mlx_init()) == NULL)
		return (1);
	if ((e->win_ptr = mlx_new_window(e->mlx_ptr, SCR_X, SCR_Y, "rtv1"))
		== NULL)
		return (1);
	e->img_ptr = mlx_new_image(e->mlx_ptr, SCR_X, SCR_Y);
	e->img_data = mlx_get_data_addr(e->img_ptr, &(e->bpp), &(e->sl), &(e->end));
	trace(e);
	mlx_expose_hook(e->win_ptr, expose_hook, e);
	mlx_key_hook(e->win_ptr, key_hook, e);
	mlx_loop(e->mlx_ptr);
	return (0);
}
