/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scenes2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	scene_5_bis(t_env *e)
{
	add_cone(e);
	set_vec(&e->cones->pos, 3.0, -8.0, 12.0);
	set_vec(&e->cones->dir, 0.0, 1.0, 0.0);
	e->cones->dir = normalize(&e->cones->dir);
	e->cones->angle = 0.3;
	e->cones->color = 0xF0960F;
	add_plane(e);
	set_vec(&e->planes->pos, 0.0, -1.0, 0.0);
	e->planes->d = 1.5;
	e->planes->color = 0x666666;
	add_plane(e);
	set_vec(&e->planes->next->pos, 0.0, 0.0, -1.0);
	e->planes->next->d = 20.0;
	e->planes->next->color = 0x666666;
	add_plane(e);
	set_vec(&e->planes->next->next->pos, -1.0, 0.0, 0.0);
	e->planes->next->next->d = 7.0;
	e->planes->next->next->color = 0x666666;
	e->light->pos.x = -9.0;
	e->light->pos.y = -2.0;
	e->light->pos.z = 4.0;
}
