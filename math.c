/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

double	quadratic(double tab[3])
{
	double	delta;
	double	dist;
	double	t1;
	double	t2;

	dist = 0.0;
	delta = tab[1] * tab[1] - 4.0 * tab[0] * tab[2];
	if (delta > 0)
	{
		t1 = (-1.0 * tab[1] + sqrt(delta)) / (2.0 * tab[0]);
		t2 = (-1.0 * tab[1] - sqrt(delta)) / (2.0 * tab[0]);
		dist = t1 < t2 ? t1 : t2;
	}
	return (dist);
}

void	fill_collision(t_env *e, void *ptr, double dist, int shape)
{
	e->coll = 1;
	e->clsn->impact_d = dist;
	e->clsn->object = ptr;
	e->clsn->impact.x = e->ray->origin.x + e->ray->dir.x * dist;
	e->clsn->impact.y = e->ray->origin.y + e->ray->dir.y * dist;
	e->clsn->impact.z = e->ray->origin.z + e->ray->dir.z * dist;
	if (shape == SPHERE)
		e->clsn->diffuse = &diffuse_sphere;
	if (shape == PLANE)
		e->clsn->diffuse = &diffuse_plane;
	if (shape == CONE)
		e->clsn->diffuse = &diffuse_cone;
	if (shape == CYLINDER)
		e->clsn->diffuse = &diffuse_cylinder;
}

void	calculate_normale_cylinder(t_env *e, double m)
{
	e->clsn->normale.x = e->clsn->impact.x -
	((t_cylinder *)(e->clsn->object))->pos.x -
	((t_cone *)(e->clsn->object))->dir.x * m;
	e->clsn->normale.y = e->clsn->impact.y -
	((t_cylinder *)(e->clsn->object))->pos.y -
	((t_cone *)(e->clsn->object))->dir.y * m;
	e->clsn->normale.z = e->clsn->impact.z -
	((t_cylinder *)(e->clsn->object))->pos.z -
	((t_cone *)(e->clsn->object))->dir.z * m;
}

void	calculate_normale_cone(t_env *e, double m)
{
	e->clsn->normale.x = e->clsn->impact.x -
	((t_cone *)(e->clsn->object))->pos.x - (1.0 *
		pow(((t_cone *)(e->clsn->object))->angle, 2.0)) *
	((t_cone *)(e->clsn->object))->dir.x * m;
	e->clsn->normale.y = e->clsn->impact.y -
	((t_cone *)(e->clsn->object))->pos.y - (1.0 *
		pow(((t_cone *)(e->clsn->object))->angle, 2.0)) *
	((t_cone *)(e->clsn->object))->dir.y * m;
	e->clsn->normale.z = e->clsn->impact.z -
	((t_cone *)(e->clsn->object))->pos.z - (1.0 *
		pow(((t_cone *)(e->clsn->object))->angle, 2.0)) *
	((t_cone *)(e->clsn->object))->dir.z * m;
}
