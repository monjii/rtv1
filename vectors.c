/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	set_vec(t_vec *vec, double x, double y, double z)
{
	vec->x = x;
	vec->y = y;
	vec->z = z;
}

double	magnitude(t_vec *vec)
{
	double	len;

	len = sqrt(pow(vec->x, 2.0) + pow(vec->y, 2.0) + pow(vec->z, 2.0));
	return (len);
}

t_vec	normalize(t_vec *vec)
{
	double	len;
	t_vec	new_vec;

	len = magnitude(vec);
	new_vec.x = vec->x / len;
	new_vec.y = vec->y / len;
	new_vec.z = vec->z / len;
	return (new_vec);
}

t_vec	sub_vec(t_vec *vec1, t_vec *vec2)
{
	t_vec	new_vec;

	new_vec.x = vec1->x - vec2->x;
	new_vec.y = vec1->y - vec2->y;
	new_vec.z = vec1->z - vec2->z;
	return (new_vec);
}

double	dot_product(t_vec *a, t_vec *b)
{
	double	res;

	res = a->x * b->x + a->y * b->y + a->z * b->z;
	return (res);
}
