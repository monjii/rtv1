/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   impact.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	impact(t_env *e)
{
	if (e->spheres)
		impact_sphere(e);
	if (e->planes)
		impact_plane(e);
	if (e->cylinders)
		impact_cylinder(e);
	if (e->cones)
		impact_cone(e);
}

void	impact_sphere(t_env *e)
{
	double		res[3];
	double		t[2];
	double		dist;
	t_sphere	*ptr;

	ptr = e->spheres;
	while (ptr != NULL)
	{
		res[0] = pow(e->ray->dir.x, 2.0) + pow(e->ray->dir.y, 2.0) +
			pow(e->ray->dir.z, 2.0);
		res[1] = 2.0 * (e->ray->dir.x * (e->ray->origin.x - ptr->pos.x) +
			e->ray->dir.y * (e->ray->origin.y - ptr->pos.y) + e->ray->dir.z *
				(e->ray->origin.z - ptr->pos.z));
		res[2] = (pow(e->ray->origin.x - ptr->pos.x, 2.0) +
			pow(e->ray->origin.y - ptr->pos.y, 2.0) + pow(e->ray->origin.z -
				ptr->pos.z, 2.0)) - pow(ptr->radius, 2.0);
		if ((dist = quadratic(res)) > 0)
			if ((e->clsn->impact_d < 0.0 || dist < e->clsn->impact_d) && dist
				> 0.0)
				fill_collision(e, ptr, dist, SPHERE);
		ptr = ptr->next;
	}
}

void	impact_plane(t_env *e)
{
	double		dist;
	t_plane		*ptr;

	ptr = e->planes;
	while (ptr != NULL)
	{
		dist = -1.0 * (dot_product(&ptr->pos, &e->ray->origin) + ptr->d) /
			(ptr->pos.x * e->ray->dir.x + ptr->pos.y * e->ray->dir.y +
				ptr->pos.z * e->ray->dir.z);
		if ((e->clsn->impact_d < 0.0 || dist < e->clsn->impact_d) && dist
			> 0.0)
			fill_collision(e, ptr, dist, PLANE);
		ptr = ptr->next;
	}
}

void	impact_cylinder(t_env *e)
{
	double		res[3];
	double		dist;
	t_vec		point;
	t_cylinder	*ptr;

	ptr = e->cylinders;
	while (ptr != NULL)
	{
		point = sub_vec(&e->ray->origin, &ptr->pos);
		res[0] = dot_product(&e->ray->dir, &e->ray->dir) -
			pow(dot_product(&e->ray->dir, &ptr->dir), 2.0);
		res[1] = 2.0 * (dot_product(&e->ray->dir, &point) -
			dot_product(&e->ray->dir, &ptr->dir) *
				dot_product(&point, &ptr->dir));
		res[2] = dot_product(&point, &point) -
			pow(dot_product(&point, &ptr->dir), 2.0) - pow(ptr->radius, 2.0);
		if ((dist = quadratic(res)) > 0)
			if ((e->clsn->impact_d < 0.0 || dist < e->clsn->impact_d) && dist
				> 0.0)
				fill_collision(e, ptr, dist, CYLINDER);
		ptr = ptr->next;
	}
}

void	impact_cone(t_env *e)
{
	double		res[3];
	double		dist;
	t_vec		point;
	t_cone		*ptr;

	ptr = e->cones;
	while (ptr != NULL)
	{
		point = sub_vec(&e->ray->origin, &ptr->pos);
		res[0] = dot_product(&e->ray->dir, &e->ray->dir) - (1.0 + ptr->angle *
			ptr->angle) * dot_product(&e->ray->dir, &ptr->dir) *
				dot_product(&e->ray->dir, &ptr->dir);
		res[1] = 2.0 * (dot_product(&e->ray->dir, &point) - (1.0 + ptr->angle *
			ptr->angle) * dot_product(&e->ray->dir, &ptr->dir) *
				dot_product(&point, &ptr->dir));
		res[2] = dot_product(&point, &point) - (1.0 + ptr->angle *
			ptr->angle) * dot_product(&point, &ptr->dir) *
				dot_product(&point, &ptr->dir);
		if ((dist = quadratic(res)) > 0)
			if ((e->clsn->impact_d < 0.0 || dist < e->clsn->impact_d) && dist
				> 0.0)
				fill_collision(e, ptr, dist, CONE);
		ptr = ptr->next;
	}
}
