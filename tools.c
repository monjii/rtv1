/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	usage(char *error, int flag)
{
	ft_putendl(error);
	if (flag == 1)
	{
		ft_putendl("     where x is a number between 1-5\n       1 : sphere");
		ft_putendl("       2 : cylinder\n       3 : cone\n       4 : plane");
		ft_putendl("       5 : all objects\n");
	}
	exit(0);
}

void	parser(int argc, char **argv, t_env *e)
{
	if (argc != 2)
		usage("\nPlease choose the scene like below :\n    ./rtv1 x", 1);
	if ((argv[1][0] != '1' && argv[1][0] != '2' && argv[1][0] != '3' &&
			argv[1][0] != '4' && argv[1][0] != '5') || argv[1][1] != '\0')
		usage("\nPlease choose the scene like below :\n    ./rtv1 x", 1);
	e->n_scene = ft_atoi(argv[1]);
}

void	quit_program(char *str, t_env *e)
{
	mlx_destroy_image(e->mlx_ptr, e->img_ptr);
	mlx_destroy_window(e->mlx_ptr, e->win_ptr);
	usage(str, 0);
}

void	put_pixel_to_img(t_env *e, int x, int y, int color)
{
	int		index;

	index = y * e->sl + x * 4;
	e->img_data[index] = color;
	e->img_data[index + 1] = color >> 8;
	e->img_data[index + 2] = color >> 16;
}
