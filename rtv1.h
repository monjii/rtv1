/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:53 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:37:55 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H
# include "minilibX/mlx.h"
# include "libft/includes/libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <math.h>

# define HORIZON_ANGLE 0.0
# define VERTICAL_ANGLE 0.0
# define KEY_ESC 53
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_UP 126
# define KEY_DOWN 125
# define SCR_X 1200
# define SCR_Y 800
# define SPHERE 0
# define CONE 1
# define PLANE 2
# define CYLINDER 3

typedef struct	s_vec
{
	double				x;
	double				y;
	double				z;
}				t_vec;

typedef struct	s_camera
{
	t_vec				pos;
	t_vec				dir;
}				t_camera;

typedef struct	s_light
{
	t_vec				pos;
}				t_light;

typedef struct	s_ray
{
	t_vec				origin;
	t_vec				dir;
}				t_ray;

typedef struct	s_collision
{
	t_vec				ray_dir;

	void				*object;
	void				(*diffuse)();

	t_vec				impact;
	t_vec				normale;
	double				impact_d;
	double				dist_tolight;

	double				light_ratio;
}				t_collision;

typedef struct	s_sphere
{
	t_vec				pos;
	double				radius;
	int					color;

	struct s_sphere		*next;
}				t_sphere;

typedef struct	s_plane
{
	t_vec				pos;
	double				d;
	int					color;

	struct s_plane		*next;
}				t_plane;

typedef struct	s_cylinder
{
	t_vec				pos;
	t_vec				dir;
	double				radius;
	double				z_min;
	double				z_max;
	int					color;

	struct s_cylinder	*next;
}				t_cylinder;

typedef struct	s_cone
{
	t_vec				pos;
	t_vec				dir;
	double				angle;
	int					color;

	struct s_cone		*next;
}				t_cone;

typedef struct	s_env
{
	void				*img_ptr;
	char				*img_data;
	void				*mlx_ptr;
	void				*win_ptr;

	int					sl;
	int					bpp;
	int					end;

	int					n_scene;
	double				ambiant_light;
	t_camera			*camera;
	t_light				*light;
	t_ray				*ray;
	t_sphere			*spheres;
	t_plane				*planes;
	t_cylinder			*cylinders;
	t_cone				*cones;
	t_collision			*clsn;

	int					coll;
}				t_env;

/*
**	main.c
*/
void			init(t_env *e);
void			init_scene(t_env *e);
int				expose_hook(t_env *e);
int				key_hook(int keycode, t_env *e);

/*
**	list.c
*/
void			add_sphere(t_env *e);
void			add_cylinder(t_env *e);
void			add_cone(t_env *e);
void			add_plane(t_env *e);

/*
**	impact.c
*/
void			impact(t_env *e);
void			impact_sphere(t_env *e);
void			impact_plane(t_env *e);
void			impact_cylinder(t_env *e);
void			impact_cone(t_env *e);

/*
**	sub_impact.c
*/
int				sub_impact(t_env *e, t_vec *origin, t_vec *dir);
int				sub_impact_sphere(t_env *e, t_vec *origin, t_vec *dir);
int				sub_impact_plane(t_env *e, t_vec *origin, t_vec *dir);
int				sub_impact_cylinder(t_env *e, t_vec *origin, t_vec *dir);
int				sub_impact_cone(t_env *e, t_vec *origin, t_vec *dir);

/*
**	reflect.c
*/
void			diffuse_sphere(t_env *e);
void			diffuse_plane(t_env *e);
void			diffuse_cylinder(t_env *e);
void			diffuse_cone(t_env *e);

/*
**	trace.c
*/
int				get_color(t_env *e, int color);
void			get_ray_dir(t_env *e, t_vec *vec, int x, int y);
void			draw_pixel(t_env *e, int x, int y);
void			trace(t_env *e);

/*
**	math.c
*/
double			quadratic(double tab[3]);
void			fill_collision(t_env *e, void *ptr, double dist, int shape);
void			calculate_normale_cylinder(t_env *e, double m);
void			calculate_normale_cone(t_env *e, double m);

/*
**	vectors.c
*/
void			set_vec(t_vec *vec, double x, double y, double z);
double			magnitude(t_vec *vec);
t_vec			normalize(t_vec *vec);
t_vec			sub_vec(t_vec *vec1, t_vec *vec2);
double			dot_product(t_vec *a, t_vec *b);

/*
**	tools.c
*/
void			usage(char *error, int flag);
void			quit_program(char *str, t_env *e);
void			parser(int argc, char **argv, t_env *e);
void			put_pixel_to_img(t_env *e, int x, int y, int color);

/*
**	scenes.c scenes2.c
*/
void			scene_1(t_env *e);
void			scene_2(t_env *e);
void			scene_3(t_env *e);
void			scene_4(t_env *e);
void			scene_5(t_env *e);
void			scene_5_bis(t_env *e);

#endif
