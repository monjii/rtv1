/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub_impact.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		sub_impact(t_env *e, t_vec *origin, t_vec *dir)
{
	int		impact;

	impact = 0;
	if (e->spheres)
		impact += sub_impact_sphere(e, origin, dir);
	if (e->planes)
		impact += sub_impact_plane(e, origin, dir);
	if (e->cylinders)
		impact += sub_impact_cylinder(e, origin, dir);
	if (e->cones)
		impact += sub_impact_cone(e, origin, dir);
	return (impact > 0 ? 1 : 0);
}

int		sub_impact_sphere(t_env *e, t_vec *origin, t_vec *dir)
{
	double		dist;
	double		res[3];
	t_sphere	*ptr;

	ptr = e->spheres;
	while (ptr != NULL)
	{
		res[0] = pow(dir->x, 2.0) + pow(dir->y, 2.0) + pow(dir->z, 2.0);
		res[1] = 2.0 * (dir->x * (origin->x - ptr->pos.x) + dir->y *
			(origin->y - ptr->pos.y) + dir->z * (origin->z - ptr->pos.z));
		res[2] = (pow(origin->x - ptr->pos.x, 2.0) + pow(origin->y -
			ptr->pos.y, 2.0) + pow(origin->z - ptr->pos.z, 2.0)) -
				pow(ptr->radius, 2.0);
		if ((dist = quadratic(res)) > 0)
			if (dist > 0.0 && dist < e->clsn->dist_tolight && (void *)ptr !=
				(e->clsn->object))
				return (1);
		ptr = ptr->next;
	}
	return (0);
}

int		sub_impact_plane(t_env *e, t_vec *origin, t_vec *dir)
{
	double		dist;
	t_plane		*ptr;

	ptr = e->planes;
	while (ptr != NULL)
	{
		dist = -1.0 * (dot_product(&ptr->pos, origin) + ptr->d) /
			(ptr->pos.x * dir->x + ptr->pos.y * dir->y + ptr->pos.z * dir->z);
		if (dist > 0.0 && dist < e->clsn->dist_tolight && (void *)ptr !=
			(e->clsn->object))
			return (1);
		ptr = ptr->next;
	}
	return (0);
}

int		sub_impact_cylinder(t_env *e, t_vec *origin, t_vec *dir)
{
	double		res[3];
	double		dist;
	t_cylinder	*ptr;
	t_vec		point;

	ptr = e->cylinders;
	while (ptr != NULL)
	{
		point = sub_vec(origin, &ptr->pos);
		res[0] = dot_product(dir, dir) - pow(dot_product(dir, &ptr->dir), 2.0);
		res[1] = 2.0 * (dot_product(dir, &point) -
			dot_product(dir, &ptr->dir) * dot_product(&point, &ptr->dir));
		res[2] = dot_product(&point, &point) -
			pow(dot_product(&point, &ptr->dir), 2.0) - pow(ptr->radius, 2.0);
		if ((dist = quadratic(res)) > 0)
			if (dist > 0.0 && dist < e->clsn->dist_tolight && (void *)ptr !=
				(e->clsn->object))
				return (1);
		ptr = ptr->next;
	}
	return (0);
}

int		sub_impact_cone(t_env *e, t_vec *origin, t_vec *dir)
{
	double		res[3];
	double		dist;
	t_cone		*ptr;
	t_vec		point;

	ptr = e->cones;
	while (ptr != NULL)
	{
		point = sub_vec(origin, &ptr->pos);
		res[0] = dot_product(dir, dir) - (1.0 + ptr->angle * ptr->angle) *
			dot_product(dir, &ptr->dir) * dot_product(dir, &ptr->dir);
		res[1] = 2.0 * (dot_product(dir, &point) - (1.0 + ptr->angle *
			ptr->angle) * dot_product(dir, &ptr->dir) *
				dot_product(&point, &ptr->dir));
		res[2] = dot_product(&point, &point) - (1.0 + ptr->angle *
			ptr->angle) * dot_product(&point, &ptr->dir) *
		dot_product(&point, &ptr->dir);
		if ((dist = quadratic(res)) > 0)
			if (dist > 0.0 && dist < e->clsn->dist_tolight && (void *)ptr !=
				(e->clsn->object))
				return (1);
		ptr = ptr->next;
	}
	return (0);
}
