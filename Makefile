NAME = rtv1

FLAGS = -Wall -Wextra -Werror

LIBFT = -L./libft/ -lft

MLX = -L./minilibX/ -lmlx -framework OpenGL -framework AppKit

SRCS = main.c list.c impact.c sub_impact.c diffuse.c trace.c math.c vectors.c tools.c scenes.c scenes2.c

SRCO = $(SRCS:%.c=%.o)

all: $(NAME)

$(NAME): $(SRCO)
	@make -C libft/
	@gcc -o $(NAME) $(FLAGS) $(LIBFT) $(MLX) $(SRCO)

$(SRCO):
	@gcc -c $(SRCS) $<

clean:
	@make -C libft/ clean
	@/bin/rm -f $(SRCO)

fclean: clean
	@make -C libft/ fclean
	@/bin/rm -f $(NAME)

re: fclean all

prog:
	@gcc -o $(NAME) $(FLAGS) $(LIBFT) $(MLX) $(SRCS)
mlx:
	make -C minilibX/
