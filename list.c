/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	add_sphere(t_env *e)
{
	t_sphere	*ptr;
	t_sphere	*new_sphere;

	new_sphere = (t_sphere *)malloc(sizeof(t_sphere));
	if (new_sphere == NULL)
		usage("Malloc error", 0);
	new_sphere->next = NULL;
	ptr = e->spheres;
	if (e->spheres == NULL)
		e->spheres = new_sphere;
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_sphere;
	}
}

void	add_cylinder(t_env *e)
{
	t_cylinder	*ptr;
	t_cylinder	*new_cylinder;

	new_cylinder = (t_cylinder *)malloc(sizeof(t_cylinder));
	if (new_cylinder == NULL)
		usage("Malloc error", 0);
	new_cylinder->next = NULL;
	ptr = e->cylinders;
	if (e->cylinders == NULL)
		e->cylinders = new_cylinder;
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_cylinder;
	}
}

void	add_cone(t_env *e)
{
	t_cone	*ptr;
	t_cone	*new_cone;

	new_cone = (t_cone *)malloc(sizeof(t_cone));
	if (new_cone == NULL)
		usage("Malloc error", 0);
	new_cone->next = NULL;
	ptr = e->cones;
	if (e->cones == NULL)
		e->cones = new_cone;
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_cone;
	}
}

void	add_plane(t_env *e)
{
	t_plane	*ptr;
	t_plane	*new_plane;

	new_plane = (t_plane *)malloc(sizeof(t_plane));
	if (new_plane == NULL)
		usage("Malloc error", 0);
	new_plane->next = NULL;
	ptr = e->planes;
	if (e->planes == NULL)
		e->planes = new_plane;
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_plane;
	}
}
