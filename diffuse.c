/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diffuse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	diffuse_sphere(t_env *e)
{
	t_vec	light;
	double	cos_s;

	set_vec(&e->clsn->normale, e->clsn->impact.x -
	((t_sphere *)(e->clsn->object))->pos.x, e->clsn->impact.y -
	((t_sphere *)(e->clsn->object))->pos.y, e->clsn->impact.z -
	((t_sphere *)(e->clsn->object))->pos.z);
	e->clsn->normale = normalize(&e->clsn->normale);
	light = sub_vec(&e->light->pos, &e->clsn->impact);
	e->clsn->dist_tolight = magnitude(&light);
	light = normalize(&light);
	cos_s = dot_product(&e->clsn->normale, &light);
	if (cos_s < 0.0)
		cos_s = 0.0;
	e->clsn->light_ratio = e->ambiant_light + cos_s * (1.0 - e->ambiant_light);
	if (sub_impact(e, &e->clsn->impact, &light))
		e->clsn->light_ratio = e->ambiant_light;
}

void	diffuse_plane(t_env *e)
{
	t_vec	light;
	double	cos_s;

	light = sub_vec(&e->light->pos, &e->clsn->impact);
	e->clsn->dist_tolight = magnitude(&light);
	light = normalize(&light);
	cos_s = dot_product(&(((t_plane *)(e->clsn->object))->pos), &light);
	if (cos_s < 0.0)
		cos_s = 0.0;
	e->clsn->light_ratio = e->ambiant_light + cos_s * (1.0 - e->ambiant_light);
	if (sub_impact(e, &e->clsn->impact, &light))
		e->clsn->light_ratio = e->ambiant_light;
}

void	diffuse_cylinder(t_env *e)
{
	t_vec	light;
	t_vec	pt;
	double	m;
	double	cos_s;

	pt = sub_vec(&e->ray->origin, &((t_cone *)(e->clsn->object))->pos);
	m = dot_product(&e->ray->dir, &((t_cylinder *)(e->clsn->object))->dir) *
	e->clsn->impact_d + dot_product(&pt, &((t_cone *)(e->clsn->object))->dir);
	calculate_normale_cylinder(e, m);
	e->clsn->normale = normalize(&e->clsn->normale);
	light = sub_vec(&e->light->pos, &e->clsn->impact);
	e->clsn->dist_tolight = magnitude(&light);
	light = normalize(&light);
	cos_s = dot_product(&e->clsn->normale, &light);
	if (cos_s < 0.0)
		cos_s = 0.0;
	e->clsn->light_ratio = e->ambiant_light + cos_s * (1.0 - e->ambiant_light);
	if (sub_impact(e, &e->clsn->impact, &light))
		e->clsn->light_ratio = e->ambiant_light;
}

void	diffuse_cone(t_env *e)
{
	t_vec	light;
	double	m;
	t_vec	pt;
	double	cos_s;

	pt = sub_vec(&e->ray->origin, &((t_cone *)(e->clsn->object))->pos);
	m = dot_product(&e->ray->dir, &((t_cone *)(e->clsn->object))->dir) *
	e->clsn->impact_d + dot_product(&pt, &((t_cone *)(e->clsn->object))->dir);
	calculate_normale_cone(e, m);
	e->clsn->normale = normalize(&e->clsn->normale);
	light = sub_vec(&e->light->pos, &e->clsn->impact);
	e->clsn->dist_tolight = magnitude(&light);
	light = normalize(&light);
	cos_s = dot_product(&e->clsn->normale, &light);
	if (cos_s < 0.0)
		cos_s = 0.0;
	e->clsn->light_ratio = e->ambiant_light + cos_s * (1.0 - e->ambiant_light);
	if (sub_impact(e, &e->clsn->impact, &light))
		e->clsn->light_ratio = e->ambiant_light;
}
