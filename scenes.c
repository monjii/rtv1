/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scenes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	scene_1(t_env *e)
{
	add_sphere(e);
	set_vec(&e->spheres->pos, 0.0, 0.0, 10.0);
	e->spheres->radius = 2.0;
	e->spheres->color = 0xCD2403;
}

void	scene_2(t_env *e)
{
	add_cylinder(e);
	set_vec(&e->cylinders->pos, 0.0, 0.0, 15.0);
	e->cylinders->radius = 3.0;
	set_vec(&e->cylinders->dir, 0.0, 1.0, 0.0);
	e->cylinders->color = 0x046380;
}

void	scene_3(t_env *e)
{
	add_cone(e);
	set_vec(&e->cones->pos, 0.0, -6.0, 9.0);
	set_vec(&e->cones->dir, 0.0, 1.0, 0.0);
	e->cones->dir = normalize(&e->cones->dir);
	e->cones->angle = 0.40;
	e->cones->color = 0xF0960F;
	e->light->pos.x = 43.0;
	e->light->pos.y = 8.0;
}

void	scene_4(t_env *e)
{
	add_plane(e);
	set_vec(&e->planes->pos, 0.0, 0.0, -1.0);
	e->planes->d = 20.0;
	e->planes->color = 0x666666;
	add_plane(e);
	set_vec(&e->planes->next->pos, 0.0, -1.0, 0.0);
	e->planes->next->d = 1.0;
	e->planes->next->color = 0x666666;
}

void	scene_5(t_env *e)
{
	add_sphere(e);
	set_vec(&e->spheres->pos, -4.0, 0.5, 8.0);
	e->spheres->radius = 1.3;
	e->spheres->color = 0xFFFFFF;
	add_sphere(e);
	set_vec(&e->spheres->next->pos, -2.5, 0.5, 8.0);
	e->spheres->next->radius = 1.3;
	e->spheres->next->color = 0xC9D0D0;
	add_sphere(e);
	set_vec(&e->spheres->next->next->pos, -1.0, 0.5, 8.0);
	e->spheres->next->next->radius = 1.3;
	e->spheres->next->next->color = 0x94A2A2;
	add_sphere(e);
	set_vec(&e->spheres->next->next->next->pos, 0.5, 0.5, 8.0);
	e->spheres->next->next->next->radius = 1.3;
	e->spheres->next->next->next->color = 0x616F6F;
	add_cylinder(e);
	set_vec(&e->cylinders->pos, -1.0, 0.5, 7.5);
	set_vec(&e->cylinders->dir, 1.0, 0.0, 0.0);
	e->cylinders->dir = normalize(&e->cylinders->dir);
	e->cylinders->radius = 0.2;
	e->cylinders->color = 0x046380;
	scene_5_bis(e);
}
