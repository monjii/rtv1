/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		get_color(t_env *e, int color)
{
	int				new_color;
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;

	r = (color >> 16);
	g = (color >> 8);
	b = color;
	r *= e->clsn->light_ratio;
	g *= e->clsn->light_ratio;
	b *= e->clsn->light_ratio;
	new_color = b;
	new_color += g << 8;
	new_color += r << 16;
	return (new_color);
}

void	get_ray_dir(t_env *e, t_vec *vec, int x, int y)
{
	double	p_w;
	double	p_h;

	p_w = (double)SCR_X / 100.0;
	p_h = (double)SCR_Y / 100.0;
	vec->x = e->ray->origin.x - p_w / 2.0 + (p_w / (double)SCR_X * (double)x);
	vec->y = e->ray->origin.y - p_h / 2.0 + (p_w / (double)SCR_X * (double)y);
	vec->z = p_w / 2.0;
	vec->x = vec->x * cos(HORIZON_ANGLE) + vec->z * sin(HORIZON_ANGLE);
	vec->y = vec->y * cos(VERTICAL_ANGLE) - vec->z * sin(VERTICAL_ANGLE);
}

void	draw_pixel(t_env *e, int x, int y)
{
	void	*obj;

	obj = e->clsn->object;
	if (e->clsn->diffuse == &diffuse_sphere)
		put_pixel_to_img(e, x, y, get_color(e, ((t_sphere *)(obj))->color));
	if (e->clsn->diffuse == &diffuse_plane)
		put_pixel_to_img(e, x, y, get_color(e, ((t_plane *)(obj))->color));
	if (e->clsn->diffuse == &diffuse_cylinder)
		put_pixel_to_img(e, x, y, get_color(e, ((t_cylinder *)(obj))->color));
	if (e->clsn->diffuse == &diffuse_cone)
		put_pixel_to_img(e, x, y, get_color(e, ((t_cone *)(obj))->color));
}

void	trace(t_env *e)
{
	t_vec	vp;
	int		x;
	int		y;

	y = 0;
	while (y < SCR_Y)
	{
		x = 0;
		while (x < SCR_X)
		{
			get_ray_dir(e, &vp, x, y);
			e->ray->dir = normalize(&vp);
			impact(e);
			if (e->coll)
			{
				e->clsn->diffuse(e);
				draw_pixel(e, x, y);
			}
			e->clsn->impact_d = -1.0;
			e->coll = 0;
			e->clsn->light_ratio = 0.0;
			x++;
		}
		y++;
	}
}
